<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/login', 'AuthController@index')->name('login');
Route::post('/sendlogin', 'AuthController@sendLoginRequest')->name('login_action');
Route::get('/logout', 'AuthController@logout')->name('logout_action');

// Route::group(['middleware' => 'auth'], function(){

Route::get('/','pasiencontroller@tampil')->name('tampil_rumkit');

// route pasien
Route::get('/pasien','pasiencontroller@index' )->name('tampil_pasien');

// route tambah pasien
Route::get('pasien/tambah','pasiencontroller@tambahpasien')->name('tambah_pasien');
Route::post('/post/tambah','pasiencontroller@postdata')->name('post_tambah');

// route edit pasien
Route::get('pasien/edit/{id}','pasiencontroller@editPasien')->name('edit_pasien');
Route::post('pasien/update/{id}','pasiencontroller@editData')->name('update_pasien');
Route::get('pasien/hapus/{id}','pasiencontroller@hapusData')->name('hapus_data');

// route medikal
Route::get('/medik','medikcontroller@index')->name('ini_medik');

// tambah medik
Route::get('medik/tambah','medikcontroller@tambahmedik')->name('tambah_medik');
Route::post('/post/medik','medikcontroller@postmedik')->name('medik_tambah');

//rote edit medik
Route::get('medik/edit/{id}','medikcontroller@editmedik')->name('edit_medik');
Route::post('medik/update/{id}','medikcontroller@updatemedik')->name('update_medik');
Route::get('medik/hapus/{id}','medikcontroller@hapusmedik')->name('hapus_medik');

// });