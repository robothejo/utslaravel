<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMedicalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical', function (Blueprint $table) {
            $table->string('no_urut');
            $table->string('diagnosa');
            $table->string('kode_pasien');
            $table->foreign('kode_pasien')->references('kode_pasien')->on('pasien');
            $table->string('kode_dokter');
            $table->foreign('kode_dokter')->references('kode_dokter')->on('dokter');
            $table->timestamps();
        });
    }



    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical');
    }
}
