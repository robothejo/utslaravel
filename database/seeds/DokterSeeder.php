<?php

use Illuminate\Database\Seeder;

class DokterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dokter')->insert(array(

            array(
                'kode_dokter'=>'KD-01',
                'nama_dokter'=>'Eman Sulaeman',
                'jenis_kelamin'=>'L',
                'telepon'=>'082212342323',
                'alamat'=>'Ciamis',
                'keahlian'=>'Jantung'
            ),
            array(
                'kode_dokter'=>'KD-02',
                'nama_dokter'=>'Sani Rianto',
                'jenis_kelamin'=>'L',
                'telepon'=>'082123400087',
                'alamat'=>'Bandung',
                'keahlian'=>'Paru-paru'
            ),
            array(
                'kode_dokter'=>'KD-03',
                'nama_dokter'=>'Lindawati',
                'jenis_kelamin'=>'P',
                'telepon'=>'082176100087',
                'alamat'=>'Bandung',
                'keahlian'=>'Kandungan'
            ),
        ));
    }
}
