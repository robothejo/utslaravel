<?php

use Illuminate\Database\Seeder;

class MedicalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medical')->insert(array(
            array(
                'no_urut'=>'1',
                'diagnosa'=>'Serangan Jantung',
                'kode_pasien'=>'KP-01',
                'kode_dokter'=>'KD-01',
            ),
            array(
                
                'no_urut'=>'2',
                'diagnosa'=>'Asma',
                'kode_pasien'=>'KP-02',
                'kode_dokter'=>'KD-02',
            ),
         ));
    }
}
