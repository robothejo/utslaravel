<?php

use Illuminate\Database\Seeder;

class rumahsakitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pasien')->insert(array(
            array(
                'kode_pasien'=>'KP-01',
                'nama_pasien'=>'Rizki',
                'jenis_kelamin'=>'L',
                'gol_darah'=>'A',
                'umur'=>'18',
                'alamat'=>'Bandung'
            ),
            array(
                'kode_pasien'=>'KP-02',
                'nama_pasien'=>'Ridho',
                'jenis_kelamin'=>'L',
                'gol_darah'=>'B',
                'umur'=>'19',
                'alamat'=>'Jakarta'
            ),
        ));
    }
}
