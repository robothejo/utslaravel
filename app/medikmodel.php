<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class medikmodel extends Model
{
    protected $table = 'medical';
    protected $fillable = ['no_urut', 'diagnosa','alamat'];

    public function haveMedical(){
        return $this->belongsTo(pasienmodel::class, 'kode_pasien', 'kode_pasien');
    }

}
