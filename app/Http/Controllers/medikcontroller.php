<?php

namespace App\Http\Controllers;
use DB;
use App\medikmodel;
use Illuminate\Http\Request;

class medikcontroller extends Controller
{
    public function index(){
        $data = medikmodel::all();
        // dd($data);
        return view('medik.medik',compact('data'));
    
    }
    public function Tambahmedik(){
        return view('medik.tambah');
    }

    public function postmedik(Request $request){
        $simpan = DB::table('medical')->insert([
            'no_urut'=>$request->no_urut,
            'diagnosa'=>$request->diagnosa,
            'kode_pasien'=>$request->kode_pasien,
            'alamat'=>$request->alamat,
            // 'is_active'=>$request->is_active,
        ]);

        return redirect('medik');
        
    }
    public function editmedik($id){
        $data = medikmodel::where('no_urut',$id)->first();

        return view('medik.edit', compact('data'));
    }
    public function updatemedik($id, Request $request){
        $data = medikmodel::where('no_urut',$id)->update([
            'diagnosa'=>$request->diagnosa,
            'kode_pasien'=>$request->kode_pasien,
            'Alamat'=>$request->Alamat,
        ]);

        return redirect('medik');
    }
    public function hapusmedik($id){
        medikmodel::where('no_urut',$id)->delete();

        return redirect('medik');
    }
}
