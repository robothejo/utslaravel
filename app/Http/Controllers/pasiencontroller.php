<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\pasienmodel;
use App\medikmodel;
use DB;

class pasiencontroller extends Controller
{
    public function index(){
        $data = pasienmodel::all();
        // dd($data);
        return view('pasien.pasien',compact('data'));
    }

    public function TambahPasien(){
        return view('pasien.tambah');
    }
     
    public function postdata(Request $request){
        $simpan = DB::table('pasien')->insert([
            'kode_pasien'=>$request->kode_pasien,
            'nama_pasien'=>$request->nama_pasien,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'gol_darah'=>$request->gol_darah,
            'umur'=>$request->umur,
        ]);
        return redirect('pasien');
    }

    public function editPasien($id){
        $data = pasienmodel::where('kode_pasien',$id)->first();

        return view('pasien.edit', compact('data'));
    }

    public function editData($id, Request $request){
        $data = pasienmodel::where('kode_pasien',$id)->update([
            'nama_pasien'=>$request->nama_pasien,
            'jenis_kelamin'=>$request->jenis_kelamin,
            'gol_darah'=>$request->gol_darah,
            'umur'=>$request->umur, 
        ]);

        return redirect('pasien');
    }

    public function hapusData($id){
        pasienmodel::where('kode_pasien',$id)->delete();

        return redirect('pasien');
    }

    public function tampil(){
        $data = pasienmodel::all();
        $medik = medikmodel::all();
        return view ('dashboard', compact('data','medik'));

    }

}