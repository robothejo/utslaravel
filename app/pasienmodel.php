<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pasienmodel extends Model
{
    protected $table = 'pasien';
    // protected $fillable = ['kode_pasien', 'nama_pasien', 'jenis_kelamin', 'gol_darah', 'umur'];

    public function hasManyMedical(){
        return $this->hasMany(medikmodel::class, 'kode_pasien', 'kode_pasien');
        
    }

}
