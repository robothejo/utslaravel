@extends('template.kolam')
@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Quick Example</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="{{route('update_pasien',$data->kode_pasien)}}">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Kode Pasien </label>
                    <input type="number" class="form-control" value="{{$data->kode_pasien}}" name="kode_pasien" readonly >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama Pasien</label>
                    <input type="text" class="form-control" value="{{$data->nama_pasien}}" name="nama_pasien">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Jenis Kelamin</label>
                    <select type="text" class="form-control" value="{{$data->jenis_kelamin}}" name="jenis_kelamin" id="jenis_kelamin">
                    <option value="L">L</option>
                    <option value="P">P</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Golongan Darah</label>
                    <select type="text" class="form-control" value="{{$data->gol_darah}}" name="gol_darah" id="gol_darah">
                    <option value="A">A</option>
                    <option value="B">B</option>
                    <option value="AB">AB</option>
                    <option value="O">O</option>
                    </select>
                  </div>
                  <!-- <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <input type="text" class="form-control" name="alamat" >
                  </div> -->
                <div class="form-group">
                    <label for="exampleInputEmail1">Umur</label>
                    <input type="text" class="form-control" value="{{$data->umur}}" name="umur" >
                </div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            @endsection