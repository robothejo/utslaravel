@extends('template.kolam')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card m-3">
            <div class="card-header">
                <a href="{{route ('tambah_pasien')}}" type="submit" class="btn btn-primary float-right">Tambah Data </a>
                <h3 class="card-title text-bold">Bordered Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered table-hover">
                    <form action="{{route('tampil_pasien')}}" method="post">
                        @csrf
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode pasien</th>
                                <th>Nama Pasien</th>
                                <th>Jenis Kelamin</th>
                                <th>Golongan Darah</th>
                                <th>Umur</th>
                                <th>Action</th>

                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $i=1;
                            @endphp
                            @foreach($data as $row)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$row->kode_pasien}}</td>
                                <td>{{$row->nama_pasien}}</td>
                                <td>{{$row->jenis_kelamin}}</td>
                                <td>{{$row->gol_darah}}</td>
                                <td>{{$row->umur}}</td>
                                <td><a href="{{route('edit_pasien',$row->kode_pasien)}}"
                                        class="btn btn-primary">Edit</a>
                                    &nbsp <a href="{{route('hapus_data',$row->kode_pasien)}}"
                                        class="btn btn-danger">Hapus</a>
                                </td>
                            <tr></tr>
                            </tr>
                            @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection