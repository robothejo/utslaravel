@extends ('template.kolam')
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card m-3">
            <div class="card-header">
                <h3 class="card-title text-bold">Tabel Data Medikal</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered table-hover">
                    <form action="#" method="post">
                        @csrf
                        <thead class="bg-info">
                            <tr>
                                <!-- <th>No</th> -->
                                <th>Nomor Urut</th>
                                <th>Nama Pasien</th>
                                <th>Diagnosa</th>
                                <th>Alamat</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $i=1;
                            @endphp
                            @foreach($medik as $row)
                            <tr>
                                <!-- <td>{{$i++}}</td> -->
                                <td>{{$row->no_urut}}</td>
                                <td>{{$row->haveMedical->nama_pasien}}</td>
                                <td>{{$row->diagnosa}}</td>
                                <td>{{$row->Alamat}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card m-3">
            <div class="card-header">
                <h3 class="card-title text-bold">Tabel Data Pasien</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered table-hover">
                    <form action="#" method="post">
                        @csrf
                        <thead class="bg-primary">
                            <tr>
                                <th>No</th>
                                <th>Kode pasien</th>
                                <th>Nama Pasien</th>
                                <th>Jenis Kelamin</th>
                                <th>Golongan Darah</th>
                                <th>Umur</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $i=1;
                            @endphp
                            @foreach($data as $row)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$row->kode_pasien}}</td>
                                <td>{{$row->nama_pasien}}</td>
                                <td>{{$row->jenis_kelamin}}</td>
                                <td>{{$row->gol_darah}}</td>
                                <td>{{$row->umur}}</td>
                            <tr></tr>
                            </tr>
                            @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
        </div>
    </div>
    <!-- Tabel Medikal -->
</div>

@endsection