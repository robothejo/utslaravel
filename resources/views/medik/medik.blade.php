@extends('template.kolam')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card m-3">
            <div class="card-header">
                <a href="{{route ('tambah_medik')}}" type="submit" class="btn btn-primary float-right">Tambah Data </a>
                <h3 class="card-title text-bold">Bordered Table</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <table class="table table-bordered table-hover">
                    <form action="{{route('tampil_pasien')}}" method="post">
                        @csrf
                        <thead>
                            <tr>
                                <th>No Urut</th>
                                <th>Diagnosa</th>
                                <th>Kode Pasien</th>
                                <th>Alamat</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                            $i=1;
                            @endphp
                            @foreach($data as $row)
                            <tr>
                                <td>{{$row->no_urut}}</td>
                                <td>{{$row->diagnosa}}</td>
                                <td>{{$row->kode_pasien}}</td>
                                <td>{{$row->Alamat}}</td>
                                <td><a href="{{route('edit_medik',$row->no_urut)}}"
                                        class="btn btn-primary">Edit</a>
                                    &nbsp <a href="{{route('hapus_medik',$row->no_urut)}}"
                                        class="btn btn-danger">Hapus</a>
                                </td>
                            <tr></tr>
                            </tr>
                            @endforeach
                        </tbody>
                    </form>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection