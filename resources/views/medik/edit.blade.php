@extends('template.kolam')
@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Quick Example</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="{{route('update_medik',$data->no_urut)}}">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nomor Urut </label>
                    <input type="number" class="form-control" value="{{$data->no_urut}}" name="no_urut" readonly >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Diagnosa</label>
                    <input type="text" class="form-control" value="{{$data->diagnosa}}" name="diagnosa">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Kode Pasien</label>
                    <input type="text" class="form-control" value="{{$data->kode_pasien}}" name="kode_pasien">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <input type="text" class="form-control" value="{{$data->Alamat}}" name="Alamat">
                  </div>
                  <!-- <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <input type="text" class="form-control" name="alamat" >
                  </div> -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            @endsection