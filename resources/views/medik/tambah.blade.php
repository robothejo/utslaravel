@extends('template.kolam')
@section('content')
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Quick Example</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form method="post" action="{{route('medik_tambah')}}">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nomor Urut</label>
                    <input type="number" class="form-control" name="no_urut" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Diagnosa</label>
                    <input type="text" class="form-control" name="diagnosa">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Kode Pasien</label>
                    <input type="text" class="form-control" name="kode_pasien">
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Alamat</label>
                    <input type="text" class="form-control" name="alamat">
                  </div>
                 <!-- <div class="form-group">
                    <label for="exampleInputEmail1">is active</label>
                    <input type="text" class="form-control" name="is_active" >
                  </div> -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            @endsection